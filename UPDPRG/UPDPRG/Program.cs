﻿using Ionic.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml;
using Tamir.SharpSsh;
using UPDPRG.Model;
using MySql.Data.MySqlClient;
namespace UPDPRG
{
    class Program
    {
        static string WebService = ConfigurationManager.AppSettings["WebService"];
        static string FTPServer = ConfigurationManager.AppSettings["FTPServer"];
        static string Local = @"D:\UPDPRG";
        static string LocalMove = @"D:\UPDPRG\Temp";
        static string LocalBackup = @"D:\Backup_Program";
        static Toko TOKO = new Toko();
        public static DataTable DataStatus = new DataTable();
        static void Main(string[] args)
        {
            if(!Directory.Exists(Local))
            {
                Directory.CreateDirectory(Local);
            }
            if (!Directory.Exists(LocalMove))
            {
                Directory.CreateDirectory(LocalMove);
            }
            if (!Directory.Exists(LocalBackup))
            {
                Directory.CreateDirectory(LocalBackup);
            }
            if (args.Length != 0)
            {
                if(args[0].ToUpper() == "-ALL")
                {
                    TOKO.TIPE = "ALL";
                }
                else if (args[0].ToUpper() == "-SIM")
                {
                    TOKO.TIPE = "SIMULASI";
                }
                else if (args[0].ToUpper() == "-IKIOSK")
                {
                    TOKO.TIPE = "IKIOSK";
                }
            }
            string file = AppDomain.CurrentDomain.BaseDirectory + "//Ionic.Zip.dll";
            if (!File.Exists(file))
            {
                GetFileRes("Ionic.Zip.dll", "Ionic.Zip.dll", AppDomain.CurrentDomain.BaseDirectory);
            }
            //Console.ForegroundColor = ConsoleColor.White;
            //Console.BackgroundColor = ConsoleColor.White;
            if(TOKO.TIPE == "IKIOSK")
            {
                GetTokoIkiosk();
                
            }
            else
            {
                TOKO.KDTK = IDM.InfoToko.Get_KodeToko();
                TOKO.NAMA = IDM.InfoToko.Get_LokasiToko();
                TOKO.CABANG = IDM.InfoToko.Get_Cabang();
            }
            /*
            TOKO.KDTK = "THXX";
            TOKO.NAMA = "TESTER";
            TOKO.CABANG = "G027";
            */
            ClearFolder(LocalMove);
            ClearFolder(Local);
            UpdateStatus("UPDATE PROGRAM V." + Assembly.GetExecutingAssembly().GetName().Version);
            UpdateStatus("Copyright © 2017 - Iwa Sugriwa");
            UpdateStatus("ALAMAT WEB SERVICE : " + WebService);
            UpdateStatus("ALAMAT FTP SERVER : " + FTPServer);
            UpdateStatus(TOKO.KDTK + " - " + TOKO.NAMA + " - " + TOKO.CABANG);
            //UpdateStatus(PostData(WebService + "//GetProgram", JsonConvert.SerializeObject(TOKO)));
            UpdateProgram();
            ClearFolder(LocalMove);
            ClearFolder(Local);
            //UpdateStatus(GetIpAspera());
            if(TOKO.TIPE != "IKIOSK")
            {
                RunPerintah();
            }
            //Console.ReadLine();
            Environment.Exit(0);
        }

        static void RunPerintah()
        {
            using (DataTable Cmd = GetPerintah())
            {
                if (Cmd.Rows.Count != 0)
                {
                    foreach(DataRow c in Cmd.Rows)
                    {
                        if(RunCMD(c["PERINTAH"].ToString(),c["ID"].ToString()))
                        {
                            ADDStatus("CMD" + c["ID"].ToString(), "OK");
                        }
                        else
                        {
                            ADDStatus("CMD" + c["ID"].ToString(), "GAGAL");
                        }
                    }
                }
            }
        }

        static void GetFileRes(string NamaFile, string NamaFileAs, string ToFolder)
        {
            try
            {
                Assembly Asm = Assembly.GetExecutingAssembly();
                Stream strm = Asm.GetManifestResourceStream("UPDPRG." + NamaFile);
                BinaryReader br = new BinaryReader(strm);
                FileStream fs = File.Create(ToFolder + "\\" + NamaFileAs);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(br.ReadBytes(checked((int)br.BaseStream.Length)));
                bw.Flush();
                fs.Close();
                bw.Close();
            }
            catch (Exception)
            {

            }
        }

        static void UpdateProgram()
        {
            using (DataTable Prg = GetProgram())
            {
                if(Prg.Rows.Count != 0)
                {
                    foreach(DataRow dr in Prg.Rows)
                    {
                        var PROGRAM = dr[0].ToString();
                        var VERSI = dr[1].ToString();
                        var FILE_ZIP = dr[2].ToString();
                        var FOLDER = "";
                        if(dr[3].ToString() == "")
                        {
                            FOLDER = "D:\\BACKOFF";
                        }
                        else
                        {
                            FOLDER = dr[3].ToString();
                        }
                        //UpdateStatus(PROGRAM + "-" + VERSI + "-" + FILE_ZIP + "-" + FOLDER);
                        if(CekVersi(PROGRAM,VERSI,FOLDER))
                        {
                            if (DownloadProgram(FILE_ZIP))
                            {
                                BackupProgram(Local + "\\" + FILE_ZIP);
                                //CopyFile(Local + "\\" + FILE_ZIP, LocalBackup);
                                string[] LIST_ZIP = GetFileZip(Local + "\\" + FILE_ZIP).Split('#');
                                foreach (string FILE_TO_COPY in LIST_ZIP)
                                {
                                    if (UnzipFile(Local + "\\" + FILE_ZIP, FILE_TO_COPY))
                                    {
                                        if (CopyFile(FILE_TO_COPY,FOLDER))
                                        {
                                            //LogUpdate(fd, "OK");
                                            UpdateStatus("UPDATE " + FILE_TO_COPY + " OK");
                                            ADDStatus(FILE_TO_COPY, "OK");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            UpdateStatus(PROGRAM + " v." + VERSI + " OK");
                        }
                    }
                }
                else
                {
                    UpdateStatus("GAGAL KONEK KE " + WebService);
                }
            }
        }

        static void BackupProgram(string Prog)
        {
            //UpdateStatus("BACKUP PROGRAM " + Prog);
            try
            {
                FileInfo fi = new FileInfo(Prog);
                File.Copy(Prog, LocalBackup + "\\" + fi.Name, true);
                //UpdateStatus("BACKUP PROGRAM " + LocalBackup + "\\" + fi.Name);
            }
            catch (Exception ex)
            {
                UpdateStatus("ERROR BACKUP" + ex.Message);
            }
        }
        
        static void ADDStatus(string PRG, string STS)
        {
            try
            {
                Status sts = new Status { KDTK = TOKO.KDTK, NAMA = TOKO.NAMA + " - " + TOKO.TIPE, PROGRAM = PRG, STATUS = STS };
                //DataStatus.Rows.Add(TOKO.KDTK,TOKO.NAMA, PROGRAM, STATUS);
                var Program = PostData(WebService + "//SendStatus", JsonConvert.SerializeObject(sts));
            }
            catch (Exception)
            {

            }
        }
        /*
        static void SendStatus()
        {
            try
            {
                string Send = JsonConvert.SerializeObject(DataStatus, Newtonsoft.Json.Formatting.Indented);
                string Respond = PostData(WebService + "//SendStatus", Send);
                UpdateStatus("KIRIM STATUS PROGRAM OK");
            }
            catch (Exception ex)
            {
                UpdateStatus("ERROR KIRIM STATUS " +  ex.Message);
            }
            finally
            {
                DataStatus.Clear();
            }
        }
        */
        private enum ExitCode
        {
            Success = 0,
            InvalidUsage = -1
        }
        static bool CopyFile(string FILE,string FOLDER)
        {
            UpdateStatus("COPY FILE " + FILE);
            int a = 1;
            ulangCopy:
            a += 1;
            try
            {
                if (!Directory.Exists(FOLDER))
                {
                    Directory.CreateDirectory(FOLDER);
                }
                if (File.Exists(FOLDER + "\\" + FILE))
                {
                    File.SetAttributes(FOLDER + "\\" + FILE, FileAttributes.Normal);
                    if (File.Exists(FOLDER + "\\" + FILE + "_UPD"))
                    {
                        int b = 1;
                    ulangDel:
                        b += 1;
                        try
                        {
                            File.SetAttributes(FOLDER + "\\" + FILE + "_UPD", FileAttributes.Normal);
                            File.Delete(FOLDER + "\\" + FILE + "_UPD");
                        }
                        catch (Exception err)
                        {
                            if (b <= 30)
                            {
                                goto ulangDel;
                            }
                            ADDStatus(FILE, "HAPUS FILE UPD " + err.Message);
                            MoveFile(FOLDER + "\\" + FILE + "_UPD");
                        }
                    }
                    File.Move(FOLDER + "\\" + FILE, FOLDER + "\\" + FILE + "_UPD");
                }
                File.Copy(Local + "\\" + FILE, FOLDER + "\\" + FILE);
                return true;
            }
            catch (Exception err)
            {
                if (a <= 30)
                {
                    goto ulangCopy;
                }
                ADDStatus(FILE, "COPY FILE " + err.Message);
                UpdateStatus("GAGAL COPY FILE " + FILE);
                return false;
            }
        }

        static void MoveFile(string file)
        {
            FileInfo fi = new FileInfo(file);
            int a = 1;
        ulangMove:
            a += 1;
            try
            {
                File.Move(file, LocalMove + "\\" + fi.Name + "_UPD");
            }
            catch (Exception err)
            {
                if (a <= 30)
                {
                    goto ulangMove;
                }
                ADDStatus(file,"MOVE FILE " + err.Message);
            }
        }

        static bool DownloadProgram(string fileZip)
        {
            Scp sshCp;
            try
            {
                UpdateStatus("DOWNLOAD FILE : " + fileZip);
                sshCp = new Scp(FTPServer, "ftptoko");
                sshCp.Password = "xftptokox";
                sshCp.Connect();
                sshCp.Get(@"/home/ftp/" + TOKO.CABANG.ToLower() + "/" + TOKO.CABANG.ToUpper() + "/UPD/" + fileZip, Local);
                return true;
            }
            catch (Exception ex)
            {
                Log(ex.Message);
                //MessageBox.Show("GAGAL " + fileZip);
                ADDStatus(fileZip, ex.Message);
                return false;
            }
        }

        static string GetFileZip(string FileZip)
        {
            string result = "";
            try
            {
                using (ZipFile zip = ZipFile.Read(FileZip))
                {
                    int a = 0;
                    foreach (ZipEntry e in zip)
                    {
                        a += 1;
                        if (a != zip.Count)
                        {
                            result += e.FileName + "#";
                        }
                        else
                        {
                            result += e.FileName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ex.ToString());
            }
            Log(result);
            return result;
        }

        static bool UnzipFile(string fileZip, string file)
        {
            try
            {
                using (ZipFile zip = ZipFile.Read(fileZip))
                {
                    ZipEntry e = zip[file];
                    e.Extract(Local, ExtractExistingFileAction.OverwriteSilently);
                }
                return true;
            }
            catch (Exception ex)
            {
                Log(ex.ToString());
                ADDStatus(fileZip, ex.Message);
                return false;
            }
        }

        static bool CekVersi(string Program, string VersiServer,string Folder)
        {
            bool result = false;
            try
            {
                //UpdateStatus("CEK VERSI PROGRAM : " + Program.ToUpper());
                string VersiLocal = "";
                if (File.Exists(Folder + "\\" + Program))
                {
                    VersiLocal = AssemblyName.GetAssemblyName(Folder + "\\" + Program).Version.ToString();
                }
                else
                {
                    VersiLocal = "TIDAK ADA";
                }
                if (VersiLocal != VersiServer)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Log(ex.Message);
                result = false;
            }
            return result;
        }


        static DataTable GetProgram()
        {
            DataTable Dt = new DataTable();
            try
            {
                var Program = PostData(WebService + "//GetProgram", JsonConvert.SerializeObject(TOKO));
                if(!Program.StartsWith("ERROR"))
                {
                    Dt = (DataTable)JsonConvert.DeserializeObject(Program, (typeof(DataTable)));
                }
            }
            catch (Exception err)
            {
                UpdateStatus("ERROR GET PROGRAM " + err.Message);
            }
            return Dt;
        }

        static DataTable GetPerintah()
        {
            DataTable Dt = new DataTable();
            try
            {
                var Program = PostData(WebService + "//GetPerintah", JsonConvert.SerializeObject(TOKO));
                //UpdateStatus(Program);
                if (!Program.StartsWith("ERROR"))
                {
                    Dt = (DataTable)JsonConvert.DeserializeObject(Program, (typeof(DataTable)));
                }
            }
            catch (Exception err)
            {
                UpdateStatus("ERROR GET PROGRAM " + err.Message);
            }
            return Dt;
        }


        static CookieContainer _cookieContainer = new CookieContainer();
        string GetData(string Url)
        {
            int x = 30;
            UlangGET:
            x += 1;
            try
            {
                WebClient proxy = new WebClient();
                byte[] data = proxy.DownloadData(Url);
                Stream stream = new MemoryStream(data);
                StreamReader reader = new StreamReader(stream);
                return reader.ReadToEnd();
            }
            catch (Exception)
            {
                if (x < 30)
                {
                    goto UlangGET;
                }
                return "ERROR";
            }
        }

        static string PostData(string Url, string Data)
        {
            int x = 30;
            UlangPOST:
            x += 1;
            string ret = string.Empty;
            try
            {
                StreamWriter requestWriter;
                ServicePointManager.Expect100Continue = false;
                var webRequest = WebRequest.Create(Url) as HttpWebRequest;
                if (webRequest != null)
                {
                    webRequest.Method = "POST";
                    webRequest.CookieContainer = _cookieContainer;
                    webRequest.ServicePoint.Expect100Continue = false;
                    webRequest.Timeout = 20000;
                    webRequest.KeepAlive = false;
                    webRequest.ContentType = "text/json";
                    using (requestWriter = new StreamWriter(webRequest.GetRequestStream()))
                    {
                        requestWriter.Write(Data);
                    }
                }
                HttpWebResponse resp = (HttpWebResponse)webRequest.GetResponse();
                Stream resStream = resp.GetResponseStream();
                StreamReader reader = new StreamReader(resStream);
                ret = reader.ReadToEnd();
            }
            catch (Exception err)
            {
                if (x < 30)
                {
                    UpdateStatus("POST DATA ULANG POST KE " + Url);
                    goto UlangPOST;
                }
                ret = "ERROR " + err.Message;
            }
            return ret;
        }


        static void UpdateStatus(string Status)
        {
            Console.WriteLine(Status);
            Log(Status);
        }

        static string GetIpAspera()
        {
            var result = "";
            try
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(@"d:\backoff\ftptoko.exe.config");
                XmlNode xnodes = xdoc.SelectSingleNode("/configuration/userSettings/FtpToko.My.MySettings");
                foreach (XmlNode xnn in xnodes.ChildNodes)
                {
                    try
                    {
                        if (xnn.Attributes[0].Value.ToString() == "Host")
                        {
                            result = xnn.SelectSingleNode("value").InnerText;
                        }
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {

            }
            return result;
        }



        static void Log(string Message)
        {
            StreamWriter sw = null;
            string log = AppDomain.CurrentDomain.BaseDirectory + "\\UPDPRG_LOG.txt";
            try
            {
                if (File.Exists(log))
                {
                    long length = new FileInfo(log).Length;
                    if (length >= 10485760)
                    {
                        File.Delete(log);
                    }
                }
                sw = new StreamWriter(log, true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {

            }
        }

        static bool RunCMD(string Perintah, string ID)
        {
            Process proc = new Process();
            int x = 30;
            UlangHapusBat:
            x += 1;
            try
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                if (File.Exists(Local + "\\CMD" + ID.ToUpper() + ".bat"))
                {
                    File.Delete(Local + "\\CMD" + ID.ToUpper() + ".bat");
                }
            }
            catch (Exception)
            {
                if (x < 30)
                {
                    goto UlangHapusBat;
                }
            }
            try
            {
                StringBuilder rd = new StringBuilder();
                rd.Append(@"@ECHO OFF");
                rd.Append("\r\n");
                rd.Append(Perintah);
                rd.Append("\r\n");
                rd.Append("exit");
                File.WriteAllText(Local + "\\CMD" + ID.ToUpper() + ".bat", rd.ToString());
                proc.EnableRaisingEvents = false;
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.StartInfo.FileName = Local + "\\CMD" + ID.ToUpper() + ".bat";
                proc.Start();
                proc.WaitForExit();
                if(proc.ExitCode == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                //return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                proc.Close();
            }

        }

        static void ClearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);
            try
            {
                foreach (FileInfo fi in dir.GetFiles())
                {
                    fi.IsReadOnly = false;
                    fi.Delete();
                }
                foreach (DirectoryInfo di in dir.GetDirectories())
                {
                    ClearFolder(di.FullName);
                    di.Delete();
                }
            }
            catch (Exception)
            {

            }
        }

        static void GetTokoIkiosk()
        {
            string Server = "server=localhost;port=3306;pooling=false;user id=ikioskterminal;password=indomaret;connection timeout=15;database=ikioskterminal";
            MySqlConnection db = new MySqlConnection(Server);
            try
            {
                db.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT CONCAT(kode_toko,'|',nama_toko,'|',gudang) FROM toko_profile;", db);
                string[] Toko = cmd.ExecuteScalar().ToString().Split('|');
                TOKO.KDTK = Toko[0];
                TOKO.NAMA = Toko[1];
                TOKO.CABANG = Toko[2];
            }
            catch (Exception)
            {

            }
            finally
            {
                db.Close();
            }
        }
    }
}
