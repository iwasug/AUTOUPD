﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WS_UPDPRG.Models;
using WS_UPDPRG.Repository;
namespace WS_UPDPRG.Controllers
{
    public class UpdateController : ApiController
    {
        UPDRepository Upd = new UPDRepository();
        [Route("GetProgram")]
        [HttpPost]
        public IHttpActionResult GetProgram([FromBody]Toko value)
        {
            var result = Upd.GetProgram(value);
            return Ok(result);
        }

        [Route("GetPerintah")]
        [HttpPost]
        public IHttpActionResult GetPerintah([FromBody]Toko value)
        {
            var result = Upd.GetPerintah(value);
            return Ok(result);
        }

        [Route("GetIpSTB")]
        [HttpPost]
        public string GetIpSTB([FromBody]Toko value)
        {
            var result = Upd.GetIpSTB(value.KDTK);
            return result;
        }

        [Route("SendStatus")]
        [HttpPost]
        public IHttpActionResult SendStatus([FromBody]Status value)
        {
            Upd.SendStatus(value);
            return Ok("OK");
        }
    }
}
