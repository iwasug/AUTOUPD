﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WS_UPDPRG.Models;
using Dapper;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.IO;

namespace WS_UPDPRG.Repository
{
    public class UPDRepository
    {
        public List<Prog> GetProgram(Toko toko)
        {
            using (IDbConnection db = new MySqlConnection(ConfigurationManager.ConnectionStrings["Koneksi"].ConnectionString))
            {
                try
                {
                    if (toko.TIPE == "ALL")
                    {
                        return db.Query<Prog>("SELECT PROGRAM,VERSI,FILE_ZIP,FOLDER FROM PROGRAM ORDER BY URUTAN;").ToList();
                    }
                    else if (toko.TIPE == "SIMULASI")
                    {
                        return db.Query<Prog>("SELECT PROGRAM,VERSI,FILE_ZIP,FOLDER FROM PROGRAM_SIMULASI WHERE RECID !='1' AND (TOKO = 'ALL' OR TOKO LIKE '%" + toko.KDTK + "%' OR TOKO IS NULL) ORDER BY URUTAN;").ToList();
                    }
                    else if (toko.TIPE == "IKIOSK")
                    {
                        return db.Query<Prog>("SELECT PROGRAM,VERSI,FILE_ZIP,FOLDER FROM PROGRAM_IKIOSK WHERE RECID !='1' AND (TOKO = 'ALL' OR TOKO LIKE '%" + toko.KDTK + "%' OR TOKO IS NULL) ORDER BY URUTAN;").ToList();
                    }
                    else
                    {
                        return db.Query<Prog>("SELECT PROGRAM,VERSI,FILE_ZIP,FOLDER FROM PROGRAM WHERE RECID !='1' AND (TOKO = 'ALL' OR TOKO LIKE '%" + toko.KDTK + "%' OR TOKO IS NULL) ORDER BY URUTAN;").ToList();
                    }
                }
                catch (Exception ex)
                {
                    Log(ex.ToString());
                    return null;
                }
            }
        }

        public List<Cmd> GetPerintah(Toko toko)
        {
            using (IDbConnection db = new MySqlConnection(ConfigurationManager.ConnectionStrings["Koneksi"].ConnectionString))
            {
                return db.Query<Cmd>("SELECT ID,PERINTAH FROM PERINTAH WHERE RECID !='1' AND (TOKO = 'ALL' OR TOKO LIKE '%" + toko.KDTK + "%' OR TOKO IS NULL);").ToList();
            }
        }

        public void SendStatus(Status STS)
        {
            string IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            using (IDbConnection db = new MySqlConnection(ConfigurationManager.ConnectionStrings["Koneksi"].ConnectionString))
            {
                string insertQuery = @"INSERT INTO `LOG`(TOKO,NAMA,PROGRAM,STATUS,UPD,IP) VALUES (@TOKO, @NAMA, @PROGRAM, @STATUS, NOW(),@IP)";
                insertQuery += "ON DUPLICATE KEY UPDATE STATUS=@STATUS,UPD=NOW(),IP=@IP;";
                var p = new DynamicParameters();
                p.Add("TOKO", STS.KDTK, DbType.String);
                p.Add("NAMA", STS.NAMA, DbType.String);
                p.Add("PROGRAM", STS.PROGRAM, DbType.String);
                p.Add("STATUS", STS.STATUS, DbType.String);
                p.Add("IP", IP, DbType.String);
                db.Execute(insertQuery, p);
            }
        }

        public string GetIpSTB(string Toko)
        {
            using (IDbConnection db = new MySqlConnection(ConfigurationManager.ConnectionStrings["Koneksi"].ConnectionString))
            {
                string Query = @"SELECT IP_STB FROM IP_STB WHERE TOKO=@TOKO;";
                var p = new DynamicParameters();
                p.Add("TOKO", Toko, DbType.String);
                return db.ExecuteScalar<string>(Query, p);
            }
        }

        static void Log(string Message)
        {
            StreamWriter sw = null;
            string log = AppDomain.CurrentDomain.BaseDirectory + "\\UPDPRG_LOG.txt";
            try
            {
                if (File.Exists(log))
                {
                    long length = new FileInfo(log).Length;
                    if (length >= 10485760)
                    {
                        File.Delete(log);
                    }
                }
                sw = new StreamWriter(log, true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {

            }
        }

    }
}