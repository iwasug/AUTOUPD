﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_UPDPRG.Models
{
    public class Status
    {
        public string KDTK { set; get; }
        public string NAMA { set; get; }
        public string PROGRAM { set; get; }
        public string STATUS { set; get; }
    }
}