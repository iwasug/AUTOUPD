﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_UPDPRG.Models
{
    public class Toko
    {
        public string KDTK { set; get; }
        public string NAMA { set; get; }
        public string CABANG { set; get; }
        public string TIPE { set; get; }
    }
}